import base64

def decode(text):
  """
  Раскодировать
  """
  return base64.b64decode(text.encode('windows-1251')).decode('windows-1251')

def encode(text):
  """
  Закодировать
  """
  return base64.b64encode(text.encode('windows-1251')).decode('windows-1251')