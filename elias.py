def decode(code):
  """
  Раскодируем элиаса
  """
  sym = code[0]

  arr = []
  drr = []

  n = 1
  while n < len(code):
    ddd = ''
    q = 1
    while code[n] == '0':
      ddd += code[n]
      q += 1
      n += 1
    sss = ''
    for i in range(q):
      sss += code[n]
      n += 1
    drr.append(ddd + sss)
    arr.append(sss)

  arr = list(map(lambda x : int(x, 2), arr))

  var = sym != '0'
  res = []
  for i in arr:
    var = not var
    res.append(('0' if var else '1') * n)
  
  hx = list(map(lambda x : int(x, 2), res))
  return hx;