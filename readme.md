## Как пользоваться?
1. Создаёте в папке проекта папку `seti`
2. Скачиваете туда все файлы
3. Используете.

```python
import seti.siberian

text = "Побу пое рм .. ."
print(seti.siberian.siberian(text)) # -> Попробуем...
```

## Что имеется

### Хемминг
Раскодируем и кодируем
```python
import seti.ham

byteStr = "1001101110"
encoded = seti.ham.encode(byteStr)
print(encoded)
decoded = seti.ham.decode(encoded)
print(decoded)
```

### Сибирский
Только раскодируем, зато все три варианта
```python
import seti.siberian

# Сибирский
print(seti.siberian.siberian("Побу пое рм .. ."))
# СибирскийХард
print(seti.siberian.siberianHard("Этиш тоол кс!с оомр п"))
# СибирскийХард+
print(seti.siberian.siberianHardPlus("Крди муоо ле к  сг- во л !оав"))
```

### Windows-1251
Кодировка такая. Иногда называется *"ASCI с русским расширением"*
```python
import seti.win

print(seti.win.windows1251(36)) # -> $
```

## Тодо
- [ ] Классы IP (A/B/C/D/E)
- [ ] Разбиение бесклассовых сетей на классы
      (ф_ф)
- [ ] Порядковые номера в сети (номер хоста)
- [ ] Что за адрес (публичный/неуникальный/ИД сети/бродкаст)
- [ ] Маски сети
- [ ] Ёмкости сети
- [ ] IPv6!
- [ ] Кодирование
- [ ] Раскодирование длинных `53 51 32 53 48 32 51 50 32 53 51 32 53 50 32 51 50 32 53 49 32 53 48 32 51 50 32 53 51 32 53 48 32 51 50 32 53 51 32 53 50 32 51 50 32`
- [x] Base64
- [x] Элиас